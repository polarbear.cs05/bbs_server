#include <stddef.h>     // size_t
#include <stdio.h>      // For standard input and output
#include <stdlib.h>     // exit()
#include <strings.h>    // bzero()
#include <errno.h>      // For error indications
#include <mysql/mysql.h>// The client API to MySQL
#include <netdb.h>      // For network database operations
#include <netinet/in.h> // Internet Protocol family
#include <signal.h>     // For signal handling
#include <sys/socket.h> // For structures needed for sockets
#include <sys/types.h>  // For data types used in system calls
#include <sys/wait.h>   // For waiting declarations
#include <unistd.h>     // Provides access to the POSIX operating system API
#include "util.h"

/* This function is called when a system call fails. 
    It displays a message about the error on stderr 
    and then aborts the program. */
void error(char *msg) { 
    perror(msg);
    exit(1);
}

/* This function is called when an error occur with MySQL. 
    It displays a message about the error on stderr 
    and then aborts the program. */
void finish_with_error(MYSQL *con){
    fprintf(stderr, "%s\n", mysql_error(con));
    mysql_close(con);
    exit(1);        
}

/* This function is used to clean up zombie processes */
void sigchld_handler(int s) {
    while(waitpid(-1, NULL, WNOHANG) > 0);
}

/* This function get sockaddr, IPv4 or IPv6 */
void* get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/* This function get port number, IPv4 or IPv6 */
unsigned short int get_in_port(struct sockaddr *sa){
    if (sa->sa_family == AF_INET) {
        return ntohs(((struct sockaddr_in*)sa)->sin_port);
    }
    return ntohs(((struct sockaddr_in6*)sa)->sin6_port);    
}

/* This function loop through linked list of addrinfo structures and 
    bind to the first feasible result */
int loop_and_bind(struct addrinfo *servinfo) {
    int sockfd, yes = 1;
    struct addrinfo *p;
    for(p = servinfo; p != NULL; p = p->ai_next) {
        /* Creates a new socket */
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) < 0) {
            perror("ERROR opening socket\n");
            continue;
        }
        
        /* Set options on socket */
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
                sizeof(int)) < 0) { error("ERROR setting socket options\n"); }

        /* Binds a socket to the address of the current host */
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) < 0) { 
            close(sockfd);
            perror("ERROR on binding\n");
            continue;
        }
        break;
    }
    if (p == NULL) { error("Failed to bind\n"); }

    return sockfd;
}

/* This function send message to socket and aborts the program 
    if an error occurs.*/
void send_cnst_msg(int sockfd, char const *msg, size_t len) {
    if (send(sockfd, msg, len, 0) < 0) { 
        error("ERROR writing to socket"); 
    }
}

/* This function set up server address and bind it to a socket, 
    the file descriptor of the socket is then be returned. 
    This function also set up the SIGCHLD signal handler. */
int initialize(char *portno) {
    int sockfd,     // File descriptor returned by the socket system call
        status;     // The status code of getaddrinfo()

    struct addrinfo hints,  /* Specify criteria that limit the set of 
                                socket addresses */
           *servinfo;       /* Point to a linked list of addrinfo structures 
                                returned by getaddrinfo() */
                                
    struct sigaction sa;    // Structure for signal handling

    /* Initializes hints to zeros */
    bzero((char *) &hints, sizeof(hints));

    /* Set criteria */
    hints.ai_family = AF_UNSPEC;        // Allow IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;    // TCP stream socket
    hints.ai_flags = AI_PASSIVE;        // For wildcard IP address 

    /* Get server address */
    if ((status = getaddrinfo(NULL, portno, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        exit(1);
    }

    /* Loop through servinfo and bind to the first feasible result */
    sockfd = loop_and_bind(servinfo);

    freeaddrinfo(servinfo);

    /* Reap all dead processes */
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) { error("ERROR on sigaction"); }

    return sockfd;
}

/* This function makes a connection to local database */
void connect_to_database(MYSQL **con) {
    *con = mysql_init(NULL); // Initialize 
    if (*con == NULL) {
        fprintf(stderr, "%s\n", mysql_error(*con));
        exit(1);
    }
    /* Connect to lacal database */
    if (mysql_real_connect(*con, "localhost", "root", "", "bbs", 
            0, NULL, 0) == NULL) {
        finish_with_error(*con);
    }
}