#ifndef CNSTMSG_INC_
#define CNSTMSG_INC_

char const COMMAND_LIST[CMD_NUM+1][MAX_CMD_NAME_LEN+1] = { {"register"},
                                                            {"login"}, 
                                                            {"logout"},
                                                            {"whoami"}, 
                                                            {"exit"}, 
                                                            {"help"},
                                                            {"create-board"}, 
                                                            {"create-post"},
                                                            {"list-board"},
                                                            {"list-post"},
                                                            {"read"},
                                                            {"delete-post"},
                                                            {"update-post"},
                                                            {"comment"}, 
                                                            {"unknown"} };

char const WELCOME_MSG[] = "********************************\n"
                           "** Welcome to the BBS server. **\n"
                           "********************************\n";

char const PROMPT[] = "% ";
                                 
char const HELP_MSG[] = "For command list, please type 'help'.\n "
                        "For command useage, type 'help [command]'.\n";
                        
char const USRNAME_EXISTENCE_STMT[] = "SELECT * FROM users WHERE username = '?'";

char const BOARD_EXISTENCE_STMT[] = "SELECT * FROM boards WHERE boardname = '?'";

char const POST_EXISTENCE_STMT[] = "SELECT author_id FROM posts WHERE id = '?'";

char const INSERT_USER_STMT[] = "INSERT INTO users"
                                "(username, email, password) "
                                "VALUES('?', '?', '?')";

char const CHECK_PASSWORD_STMT[] = "SELECT * FROM users WHERE username = '?' "
                                   "AND password = '?'";
                                   
char const INSERT_BOARD_STMT[] = "INSERT INTO boards"
                               "(boardname, moderator_id) "
                               "VALUES('?', '?')";
                               
char const INSERT_POST_STMT[] = "INSERT INTO posts"
                                "(board_id, title, content, author_id, date) "
                                "VALUES('?', '?', '?', '?', CURDATE())";

char const INSERT_COMMENT_STMT[] = "INSERT INTO "
                                   "comments(post_id, content, author_id) "
                                   "VALUES('?', '?', '?')";

char const DELETE_POST_STMT[] = "DELETE FROM posts WHERE id = '?'";

char const LIST_BOARD_STMT[] = "SELECT "
                               "boards.id, boards.boardname, users.username "
                               "FROM boards "
                               "LEFT JOIN users "
                               "ON boards.moderator_id = users.id "
                               "WHERE boards.boardname LIKE '%?%'";
                               
char const LIST_POST_STMT[] = "SELECT posts.id, posts.title, users.username, "
                              "DATE_FORMAT(date, \"%m/%d\") "
                              "FROM posts "
                              "LEFT JOIN boards ON posts.board_id = boards.id "
                              "LEFT JOIN users ON posts.author_id = users.id "
                              "WHERE posts.board_id = '?' "
                              "AND posts.title LIKE '%?%'";

char const READ_POST_STMT[] = "SELECT users.username, posts.title, posts.date, "
                              "REPLACE(posts.content, '<br>', '\n') "
                              "FROM posts "
                              "LEFT JOIN users ON posts.author_id = users.id "
                              "WHERE posts.id = '?'";

char const READ_COMMENT_STMT[] = "SELECT users.username, comments.content "
                                 "FROM comments "
                                 "LEFT JOIN users "
                                 "ON comments.author_id = users.id "
                                 "WHERE comments.post_id = '?'";
                                 
char const UPDATE_POST_STMT[] = "UPDATE posts SET ? = '?' WHERE id = '?'";

char const REGISTER_SUCCESS[] = "Register successfully.\n";

char const REGISTER_FAILED[] = "Username is already used.\n";

char const LOGIN_SUCCESS[] = "Welcome, ?.\n";

char const LOGIN_FAILED[] = "Login failed.\n" ;

char const LOGOUT_SUCCESS[] = "Bye, ?.\n";

char const CREATE_BOARD_SUCCESS[] = "Create board successfully.\n";

char const CREATE_BOARD_FAILED[] = "Board already exist.\n";

char const CREATE_POST_SUCCESS[] = "Create post successfully.\n";

char const DELETE_POST_SUCCESS[] = "Delete successfully.\n";

char const UPDATE_POST_SUCCESS[] = "Update successfully.\n";

char const COMMENT_SUCCESS[] = "Comment successfully.\n";

char const NOT_LOGIN[] = "Please login first.\n";

char const NOT_LOGOUT[] = "Please logout first.\n";

char const BOARD_NOT_EXIST[] = "Board does not exist.\n";

char const POST_NOT_EXIST[] = "Post does not exist.\n";

char const NOT_POST_OWNER[] = "Not the post owner.\n";

char const UNKNOWN_MSG[] = "Unknown command `?`\n";

char const CREATE_BOARD_USAGE[] = "Usage: create-board <name>\n";

char const LIST_BOARD_USAGE[] = "Usage: list-board [##<key>]\n";

char const CREATE_POST_USAGE[] = "Usage: create-post <board-name> --title "
                                 "<title> --content <content>\n";
                                 
char const LIST_POST_USAGE[] = "Usage: list-post <board-name> ##<key>\n";

char const READ_USAGE[] = "Usage: read <post-id>\n";

char const DELETE_POST_USAGE[] = "Usage: delete-post <post-id>\n";

char const UPDATE_POST_USAGE[] = "Usage: update-post <post-id> --title/content "
                                 "<new>\n";

char const COMMENT_USAGE[] = "Usage: comment <post-id> <comment>\n";

char const REGISTER_USAGE[] = "Usage: register <username> <email> <password>\n";

char const LOGIN_USAGE[] = "Usage: login <username> <password>\n";

#endif // CNSTMSG_INC_