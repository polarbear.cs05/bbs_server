#include <stddef.h>     // size_t
#include <stdio.h>      // For standard input and output
#include <stdlib.h>     // exit()
#include <string.h>     // For string manipulating
#include <strings.h>    // bzero()
#include <errno.h>      // For error indications
#include <mysql/mysql.h>// The client API to MySQL 
#include <netdb.h>      // For network database operations
#include <netinet/in.h> /* For constants and structures needed for 
                            internet domain addresses */
#include <sys/socket.h> // For structures needed for sockets
#include <sys/types.h>  // For data types used in system calls
#include <unistd.h>     // Provides access to the POSIX operating system API
#include "util.h"
#include "child.h"
#include "cnstmsg.inc"

void child_process(int sockfd, char *buffer, int bufsize) {
    
    struct Session session = { .LOGIN_FLG = 0, 
                               .userid[0] = '\0', 
                               .username[0] = '\0' };
    int n;          // Number of bytes received
    char tmp[512];
    /* Initialize */
    bzero(buffer, 256);
    /* Send welcome message to client */
    send_cnst_msg(sockfd, WELCOME_MSG, sizeof(WELCOME_MSG));
    send_cnst_msg(sockfd, HELP_MSG, sizeof(HELP_MSG));
    send_cnst_msg(sockfd, PROMPT, sizeof(PROMPT));
    /* recv() will block until there's something to read */
    while(n = recv(sockfd, buffer, bufsize, 0)) {
        buffer[n] = '\0';
        command_parser(sockfd, &session, buffer, tmp);
        send_cnst_msg(sockfd, PROMPT, sizeof(PROMPT));
    }
    /* Close connection */
    if (n < 0) { error("ERROR reading from socket"); }
    close(sockfd);
    exit(0);
}

void command_parser(int sockfd, struct Session *session, char *buffer, 
        char* tmp) {

    int num_fields = 0;
    struct Command cmd;
    
    /* Initialize */
    cmd.type = UNKNOWN;
    bzero(cmd.param, MAX_PARAM_NUM*(MAX_CMD_PARM_LEN + 1));    
    /* Assume every command has less then 255 bytes */
    if((cmd.filled = sscanf(buffer, "%s", cmd.param[0])) == 1) {
        /* Check command type */
        for(int i = 0; i < CMD_NUM; i++) {
            if(strcmp(cmd.param[0], COMMAND_LIST[i]) == 0) { 
                cmd.type = i;
                fill_param(buffer, &cmd);
                break;
            }
        }
        switch(cmd.type) {
            case REGISTER:
                register_cmd_handler(sockfd, tmp, &cmd); 
                break;
            case LOGIN:
                login_cmd_handler(sockfd, session, tmp, &cmd);
                break;
            case LOGOUT:
                logout_cmd_handler(sockfd, session, tmp, &cmd);
                break;
            case WHOAMI:
                whoami_cmd_handler(sockfd, session);
                break;
            case EXIT:
                close(sockfd);
                exit(0);
                break;
            case HELP:
                break;
            case CREATE_BOARD:
                create_board_handler(sockfd, tmp, session, &cmd);
                break;
            case LIST_BOARD:
                list_board_handler(sockfd, tmp, &cmd);
                break;
            case CREATE_POST:
                create_post_handler(sockfd, tmp, session, &cmd);
                break;
            case LIST_POST:
                list_post_handler(sockfd, tmp, &cmd);
                break;
            case READ:
                read_handler(sockfd, tmp, &cmd);
                break;
            case DELETE_POST:
                delete_post_handler(sockfd, tmp, session, &cmd);
                break;
            case UPDATE_POST:
                update_post_handler(sockfd, tmp, session, &cmd);
                break;
            case COMMENT:
                comment_handler(sockfd, tmp, session, &cmd);
                break;
            case UNKNOWN:
                unknown_cmd_handler(sockfd, tmp, &cmd);
                break;
            default:
                break;
        }
    }
}

void fill_param(char *buffer, struct Command *cmd) {
    switch(cmd->type) {
        case CREATE_POST:
            cmd->filled += sscanf(buffer, 
                "%*s %s --title %[^-] --content %[^\r\n]", 
                cmd->param[1], cmd->param[2], cmd->param[3]);
            *(strrchr(cmd->param[2], ' ')) = '\0';
            break;
        case LIST_BOARD:
            cmd->filled += sscanf(buffer, "%*s ##%s %s",
                cmd->param[1], cmd->param[2]);                    
            break;
        case LIST_POST:
            cmd->filled += sscanf(buffer, "%*s %s ##%s %s", 
                cmd->param[1], cmd->param[2], cmd->param[3]);               
            break;
        case UPDATE_POST:
            cmd->filled += sscanf(buffer, "%*s %s --%s %[^\r\n] %s", 
                cmd->param[1], cmd->param[2], cmd->param[3], cmd->param[4]);            
            break;
        case COMMENT:
            cmd->filled += sscanf(buffer, "%*s %s %[^\r\n]", 
                cmd->param[1], cmd->param[2]);
            break;
        default:
            cmd->filled += sscanf(buffer, "%*s %s %s %s %s", 
                cmd->param[1], cmd->param[2], cmd->param[3], cmd->param[4]);                        
            break;
    }    
}

void register_cmd_handler(int sockfd, char *stmt, struct Command *cmd) {
    MYSQL *con;
    /* Check Usage */
    if(cmd->filled != 4) {
        send_cnst_msg(sockfd, REGISTER_USAGE, sizeof(REGISTER_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, USRNAME_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(USRNAME_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Check if username is already used */
    if(mysql_num_rows(result) != 0) {
        send_cnst_msg(sockfd, REGISTER_FAILED, sizeof(REGISTER_FAILED));        
    }
    /* Create new user */
    else {
        len = bind_param(stmt, INSERT_USER_STMT, cmd, 1, 3, 
            sizeof(INSERT_USER_STMT));
        if (mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        send_cnst_msg(sockfd, REGISTER_SUCCESS, sizeof(REGISTER_SUCCESS));
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void login_cmd_handler(int sockfd, struct Session *session, char *stmt, 
        struct Command *cmd) {
    MYSQL *con;
    /* Check if logged in */
    if(session->LOGIN_FLG) {
        send_cnst_msg(sockfd, NOT_LOGOUT, sizeof(NOT_LOGOUT));
        return;
    }
    /* Check Usage */
    if(cmd->filled != 3) {
        send_cnst_msg(sockfd, LOGIN_USAGE, sizeof(LOGIN_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, CHECK_PASSWORD_STMT, cmd, 1, 2, 
        sizeof(CHECK_PASSWORD_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Login success */
    if(mysql_num_rows(result) == 1) {
        MYSQL_ROW row = mysql_fetch_row(result);
        strcpy(session->userid, row[0]);            // Set userid
        strcpy(session->username, cmd->param[1]);   // Set username
        session->LOGIN_FLG = 1;                     // Set login flag
        /* Send welcome message */
        len = bind_param(stmt, LOGIN_SUCCESS, cmd, 1, 1, sizeof(LOGIN_SUCCESS));
        send_cnst_msg(sockfd, stmt, len);
    }
    /* Login failed */
    else { send_cnst_msg(sockfd, LOGIN_FAILED, sizeof(LOGIN_FAILED)); }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void logout_cmd_handler(int sockfd, struct Session *session, char *tmp, 
        struct Command *cmd) {
    if(session->LOGIN_FLG) {
        session->LOGIN_FLG = 0; // Clear login flag
        cmd->filled = 2;
        strcpy(cmd->param[1], session->username);
        size_t len = bind_param(tmp, LOGOUT_SUCCESS, cmd, 1, 1, 
            sizeof(LOGOUT_SUCCESS));
        send_cnst_msg(sockfd, tmp, len);
    }
    else { send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN)); }    
}

void whoami_cmd_handler(int sockfd, struct Session *session) {
    if(session->LOGIN_FLG) { // Print username
        send_cnst_msg(sockfd, session->username, strlen(session->username)); 
        send_cnst_msg(sockfd, "\n", 1);
    }
    else { send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN)); }
}

void create_board_handler(int sockfd, char *stmt, struct Session *session, 
        struct Command *cmd) {
    MYSQL *con;
    /* Check if logged in */
    if(!(session->LOGIN_FLG)) {
        send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN));
        return;
    }
    /* Check Usage */
    if(cmd->filled != 2) {
        send_cnst_msg(sockfd, CREATE_BOARD_USAGE, sizeof(CREATE_BOARD_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, BOARD_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(BOARD_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    if(mysql_num_rows(result) > 0) {
        send_cnst_msg(sockfd, CREATE_BOARD_FAILED, sizeof(CREATE_BOARD_FAILED));
    }
    /* Create new board */
    else {
        strcpy(cmd->param[2], session->userid);
        cmd->filled = 3;
        size_t len = bind_param(stmt, INSERT_BOARD_STMT, cmd, 1, 2, 
            sizeof(INSERT_BOARD_STMT));
        if (mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        send_cnst_msg(sockfd, CREATE_BOARD_SUCCESS, sizeof(CREATE_BOARD_SUCCESS));
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void list_board_handler(int sockfd, char *stmt, struct Command *cmd) {
    MYSQL *con;
    /* Check Usage */
    if(cmd->filled > 2) {
        send_cnst_msg(sockfd, LIST_BOARD_USAGE, sizeof(LIST_BOARD_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, LIST_BOARD_STMT, cmd, 1, 1, 
        sizeof(LIST_BOARD_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Find max length of each column */
    size_t max_lengths[3] = {5, 4, 9}; // Length of "Index", "Name" and "Moderator"
    MYSQL_ROW row;
    while(row = mysql_fetch_row(result)) {
        size_t *lengths;
        lengths = mysql_fetch_lengths(result);
        for(int i = 0; i < 3; i++){
            if(lengths[i] > max_lengths[i]) { max_lengths[i] = lengths[i]; }
        }
    }
    /* Print out result */
    mysql_data_seek(result, 0);
    len = sprintf(stmt, " %*s | %*s | %*s \n", (int)max_lengths[0], "Index", 
        (int)max_lengths[1], "Name", (int)max_lengths[2], "Moderator");
    send_cnst_msg(sockfd, stmt, len);
    while(row = mysql_fetch_row(result)) {
        len = sprintf(stmt, " %*s | %*s | %*s \n", (int)max_lengths[0], row[0], 
            (int)max_lengths[1], row[1], (int)max_lengths[2], row[2]);
        send_cnst_msg(sockfd, stmt, len);
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void create_post_handler(int sockfd, char *stmt, struct Session *session, 
        struct Command *cmd) {
    MYSQL *con;
    /* Check if logged in */
    if(!(session->LOGIN_FLG)) {
        send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN));
        return;
    }
    /* Check Usage */
    if(cmd->filled != 4) {
        send_cnst_msg(sockfd, CREATE_POST_USAGE, sizeof(CREATE_POST_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, BOARD_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(BOARD_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Board doesn't exist */
    if(mysql_num_rows(result) == 0) {
        send_cnst_msg(sockfd, BOARD_NOT_EXIST, sizeof(BOARD_NOT_EXIST));
    }
    /* Create new Post */
    else {
        MYSQL_ROW row = mysql_fetch_row(result);
        strcpy(cmd->param[1], row[0]);          // Set board id
        strcpy(cmd->param[4], session->userid); // Set user id
        cmd->filled = 5;
        len = bind_param(stmt, INSERT_POST_STMT, cmd, 1, 4, sizeof(INSERT_POST_STMT));
        if (mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        send_cnst_msg(sockfd, CREATE_POST_SUCCESS, sizeof(CREATE_POST_SUCCESS));
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void list_post_handler(int sockfd, char *stmt, struct Command *cmd) {
    MYSQL *con;
    /* Check Usage */
    if(cmd->filled > 3 || cmd->filled < 2) {
        send_cnst_msg(sockfd, LIST_POST_USAGE, sizeof(LIST_POST_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, BOARD_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(BOARD_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Board doesn't exist */
    if(mysql_num_rows(result) == 0) {
        send_cnst_msg(sockfd, BOARD_NOT_EXIST, sizeof(BOARD_NOT_EXIST));
        /* Free resources */
        mysql_free_result(result);
        mysql_close(con); 
        return;
    }
    MYSQL_ROW row = mysql_fetch_row(result);
    strcpy(cmd->param[1], row[0]); // Set board id
    /* Setup and call MySQL query */
    len = bind_param(stmt, LIST_POST_STMT, cmd, 1, 2, sizeof(LIST_POST_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Find max length of each column */
    size_t max_lengths[4] = {2, 5, 6, 4}; // ID, Title, Author, Date
    while(row = mysql_fetch_row(result)) {
        size_t *lengths;
        lengths = mysql_fetch_lengths(result);
        for(int i = 0; i < 4; i++){
            if(lengths[i] > max_lengths[i]) { max_lengths[i] = lengths[i]; }
        }
    }
    /* Print out result */
    mysql_data_seek(result, 0);
    len = sprintf(stmt, " %*s | %*s | %*s | %*s \n", (int)max_lengths[0], "ID", 
        (int)max_lengths[1], "Title", (int)max_lengths[2], "Author", 
        (int)max_lengths[3], "Date");
    send_cnst_msg(sockfd, stmt, len);
    while (row = mysql_fetch_row(result)) {
        len = sprintf(stmt, " %*s | %*s | %*s | %*s \n", (int)max_lengths[0], 
            row[0], (int)max_lengths[1], row[1], (int)max_lengths[2], row[2], 
            (int)max_lengths[3], row[3]);
        send_cnst_msg(sockfd, stmt, len);
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);            
}

void read_handler(int sockfd, char *stmt, struct Command *cmd) {
    MYSQL *con;
    /* Check Usage */
    if(cmd->filled != 2) {
        send_cnst_msg(sockfd, READ_USAGE, sizeof(READ_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);      
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, POST_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(POST_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    /* Post doesn't exist */
    if(mysql_num_rows(result) == 0) {
        send_cnst_msg(sockfd, POST_NOT_EXIST, sizeof(POST_NOT_EXIST));
    }
    else {
        /* Print post */
        len = bind_param(stmt, READ_POST_STMT, cmd, 1, 1, 
            sizeof(READ_POST_STMT));
        if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        result = mysql_store_result(con);
        if(result == NULL) { finish_with_error(con); }
        MYSQL_ROW row = mysql_fetch_row(result);
        len = sprintf(stmt, "Author\t: %s\nTitle\t: %s\nDate\t: %s\n--\n%s\n--\n", 
            row[0], row[1], row[2], row[3]);
        send_cnst_msg(sockfd, stmt, len);
        /* Print comments */
        len = bind_param(stmt, READ_COMMENT_STMT, cmd, 1, 1, 
            sizeof(READ_COMMENT_STMT));
        if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        result = mysql_store_result(con);
        if(result == NULL) { finish_with_error(con); }
        while(row = mysql_fetch_row(result)) {
            len = sprintf(stmt, "%s: %s\n", row[0], row[1]);
            send_cnst_msg(sockfd, stmt, len); 
        }
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con); 
}

void delete_post_handler(int sockfd, char *stmt, struct Session *session, 
        struct Command *cmd) {
    MYSQL *con;
    /* Check if logged in */
    if(!(session->LOGIN_FLG)) {
        send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN));
        return;
    }
    /* Check Usage */
    if(cmd->filled != 2) {
        send_cnst_msg(sockfd, DELETE_POST_USAGE, sizeof(DELETE_POST_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, POST_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(POST_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    MYSQL_ROW row = mysql_fetch_row(result);
    /* Post doesn't exist */
    if(mysql_num_rows(result) == 0) {
        send_cnst_msg(sockfd, POST_NOT_EXIST, sizeof(POST_NOT_EXIST));
    }
    /* Not post owner */
    else if(strcmp(row[0], session->userid) != 0) {
        send_cnst_msg(sockfd, NOT_POST_OWNER, sizeof(NOT_POST_OWNER));
    }
    /* Delete post */
    else {
        len = bind_param(stmt, DELETE_POST_STMT, cmd, 1, 1, 
            sizeof(DELETE_POST_STMT));
        if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        send_cnst_msg(sockfd, DELETE_POST_SUCCESS, sizeof(DELETE_POST_SUCCESS));
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void update_post_handler(int sockfd, char *stmt, struct Session *session, 
        struct Command *cmd) {
    MYSQL *con;
    /* Check if logged in */
    if(!(session->LOGIN_FLG)) {
        send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN));
        return;
    }
    /* Check Usage */
    if(cmd->filled != 4 || 
            (strcmp(cmd->param[2], "title") != 0 && 
            strcmp(cmd->param[2], "content") != 0)) {
        send_cnst_msg(sockfd, UPDATE_POST_USAGE, sizeof(UPDATE_POST_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, POST_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(POST_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    MYSQL_ROW row = mysql_fetch_row(result);
    /* Post doesn't exist */
    if(mysql_num_rows(result) == 0) {
        send_cnst_msg(sockfd, POST_NOT_EXIST, sizeof(POST_NOT_EXIST));
    }
    /* Not post owner */
    else if(strcmp(row[0], session->userid) != 0) {
        send_cnst_msg(sockfd, NOT_POST_OWNER, sizeof(NOT_POST_OWNER));
    }
    /* Update post */
    else {
        strcpy(cmd->param[4], cmd->param[1]);
        cmd->filled = 5;
        len = bind_param(stmt, UPDATE_POST_STMT, cmd, 2, 4, 
            sizeof(UPDATE_POST_STMT));
        if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        send_cnst_msg(sockfd, UPDATE_POST_SUCCESS, sizeof(UPDATE_POST_SUCCESS));
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void comment_handler(int sockfd, char *stmt, struct Session *session,
        struct Command *cmd) {
    MYSQL *con;
    /* Check if logged in */
    if(!(session->LOGIN_FLG)) {
        send_cnst_msg(sockfd, NOT_LOGIN, sizeof(NOT_LOGIN));
        return;
    }
    /* Check Usage */
    if(cmd->filled != 3) {
        send_cnst_msg(sockfd, COMMENT_USAGE, sizeof(COMMENT_USAGE));
        return;
    }
    /* Connect to database */
    connect_to_database(&con);
    /* Setup and call MySQL query */
    size_t len = bind_param(stmt, POST_EXISTENCE_STMT, cmd, 1, 1, 
        sizeof(POST_EXISTENCE_STMT));
    if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
    /* Store result */
    MYSQL_RES *result = mysql_store_result(con);
    if(result == NULL) { finish_with_error(con); }
    MYSQL_ROW row = mysql_fetch_row(result);
    /* Post doesn't exist */
    if(mysql_num_rows(result) == 0) {
        send_cnst_msg(sockfd, POST_NOT_EXIST, sizeof(POST_NOT_EXIST));
    }
    /* Create new comment */
    else {
        strcpy(cmd->param[3], session->userid);
        cmd->filled = 4;
        len = bind_param(stmt, INSERT_COMMENT_STMT, cmd, 1, 3, 
            sizeof(INSERT_COMMENT_STMT));
        if(mysql_real_query(con, stmt, len)) { finish_with_error(con); }
        send_cnst_msg(sockfd, COMMENT_SUCCESS, sizeof(COMMENT_SUCCESS));
    }
    /* Free resources */
    mysql_free_result(result);
    mysql_close(con);
}

void unknown_cmd_handler(int sockfd, char* tmp, struct Command *cmd) {
    size_t len = bind_param(tmp, UNKNOWN_MSG, cmd, 0, 0, sizeof(UNKNOWN_MSG));
    send_cnst_msg(sockfd, tmp, len);
    send_cnst_msg(sockfd, HELP_MSG, sizeof(HELP_MSG));                        
}

/* bind parameters [i, n] */
size_t bind_param(char *stmt, char const *prepare, struct Command *cmd, int i, 
        int n, size_t size) {
    MYSQL *con;
    size_t param_len = 0;
    char *pch, *end = stmt;
    char const *head = prepare;
    
    stmt[0] = '\0';
    if(n > cmd->filled) n = cmd->filled;
    connect_to_database(&con);
    pch = strchr(prepare, '?');
    while(pch != NULL) {
        strncat(stmt, head, pch-head);
        end += pch - head;
        head = pch + 1;
        if(i <= n) {
            //strcat(stmt, cmd->param[i]);
            param_len = strlen(cmd->param[i]);
            param_len = mysql_real_escape_string_quote(con, end, cmd->param[i], 
                param_len,'\'');
            end += param_len;
            size += param_len - 1;
            i++;
        }
        pch = strchr(pch + 1, '?');
    }
    strcat(stmt, head);
    mysql_close(con);
    return (size - 1);
}