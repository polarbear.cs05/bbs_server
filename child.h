#ifndef CHILD_H_
#define CHILD_H_

#define CMD_NUM 14           // number of supported commands
#define MAX_PARAM_NUM 5      // maximum number of parameters
#define MAX_CMD_NAME_LEN 12  // maximum length of command name
#define MAX_CMD_PARM_LEN 256 // maximum length of a parameter in a command

enum CommandType {
    REGISTER, LOGIN, LOGOUT, WHOAMI, EXIT, HELP, CREATE_BOARD, CREATE_POST, 
    LIST_BOARD, LIST_POST, READ, DELETE_POST, UPDATE_POST, COMMENT, UNKNOWN
};

struct Command {
    char param[MAX_PARAM_NUM][MAX_CMD_PARM_LEN];
         
    enum CommandType type;
    int filled; // How many parameters filled
};

struct Session {
    char userid[11];
    char username[51]; // Username of logged in user
    int LOGIN_FLG;     // Set if user is logged in
};

void child_process(int sockfd, char *buffer, int bufsize);

void command_parser(int sockfd, struct Session *session, char *buffer, 
    char *tmp);
    
void fill_param(char *buffer, struct Command *cmd);

/* Register with username, email and password.
    <username> must be unique.
    <email> and <password> have no limitation. */
void register_cmd_handler(int sockfd, char *stmt, struct Command *cmd);

/* Login with username and password. */
void login_cmd_handler(int sockfd, struct Session *session, char *stmt, 
    struct Command *cmd);

/* Logout account. */
void logout_cmd_handler(int sockfd, struct Session *session, char *tmp,
    struct Command *cmd);

/* Show username if login. */
void whoami_cmd_handler(int sockfd, struct Session *session);

/* Create a new board. board name must be unique. */
void create_board_handler(int sockfd, char *stmt, struct Session *session, 
    struct Command *cmd);

/* List all boards in BBS.
    Use ##<key> to do advanced query. */
void list_board_handler(int sockfd, char *stmt, 
    struct Command *cmd);

/* Create a post which title is <title> and content is <content>.
    Use --title and --content to separate titles and content.
    <tilte> can have space but only in one line.
    <content> can have space, and key in <br> to indicate a new line.
    Must be logged in. */
void create_post_handler(int sockfd, char *stmt, struct Session *session, 
    struct Command *cmd);

/* List all posts in a board named <board-name>
    <key> is a keyword, use ## <key> to do advanced query. */
void list_post_handler(int sockfd, char *stmt, struct Command *cmd);

/* Show the post which ID is <post-id>. */
void read_handler(int sockfd, char *stmt, struct Command *cmd);

/* Delete the post which ID is <post-id>.
    Only the post owner can delete the post. */
void delete_post_handler(int sockfd, char *stmt, struct Session *session, 
    struct Command *cmd);

/* Update the post which ID is <post-id>.
    Use -- to decide which to modify, title or content, and replaced by <new>.
    Only the post owner can update the post. */
void update_post_handler(int sockfd, char *stmt, struct Session *session, 
        struct Command *cmd);

/* Leave a comment < comment > at the post which ID is <post-id> */
void comment_handler(int sockfd, char *stmt, struct Session *session, 
    struct Command *cmd);

/* Send error message. */
void unknown_cmd_handler(int sockfd, char* tmp, struct Command *cmd);

/* bind parameters [i, n] */
size_t bind_param(char *stmt, char const *prepare, struct Command *cmd, int i, 
    int n, size_t size);

#endif // CHILD_H_