/*
    REFERENCES
    
    Sockets Tutorial
    http://www.linuxhowtos.org/C_C++/socket.htm
    
    Beej's Guide to Network Programming
    http://beej-zhtw.netdpi.net/
    
    WRITTEN BY Ting-Yu CHUANG
*/
#include <stdio.h>      // For standard input and output
#include <stdlib.h>     // exit()
#include <string.h>     // For string manipulating
#include <strings.h>    // bzero()
#include <errno.h>      // For error indications
#include <arpa/inet.h>  // Convert IP addresses to text form
#include <mysql/mysql.h>// The client API to MySQL
#include <netdb.h>      // For network database operations
#include <netinet/in.h> /* For constants and structures needed for 
                            internet domain addresses */
#include <signal.h>     // For signal handling
#include <sys/socket.h> // For structures needed for sockets
#include <sys/types.h>  // For data types used in system calls
#include <unistd.h>     // Provides access to the POSIX operating system API
#include "util.h"
#include "child.h"

#define BACKLOG 10      // maximum numbers of pending connections

int main(int argc, char *argv[]) {
    int sockfd,     // File descriptor returned by the socket system call
        newsockfd;  // File descriptor returned by the accept system call

    socklen_t clilen;   // The size of the address of the client

    struct sockaddr_storage cli_addr; // The address of client

    char *portno,    // The port number on which the server accepts connections
         buffer[256],   /* The server reads characters from 
                            the socket connection into this buffer */
         s[INET6_ADDRSTRLEN];   // String of readable client IP address
         
    /* The user should pass in the port number as an argument */
    if (argc < 2) { error("ERROR no port provided"); }
    portno = argv[1];
    
    /* Set up server address and bind it to a socket
        Also set up SIGCHLD handler */
    sockfd = initialize(portno);

    /* Wait for connections */
    if (listen(sockfd, BACKLOG) < 0) { error("ERROR on listening"); }
    printf("Server: waiting for connections...\n");

    while(1){
        /* accept() will block until a client connects to the server. */
        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) { 
            perror("ERROR on accept"); 
            continue;
        }

        /* Report when new connection is established */
        inet_ntop(cli_addr.ss_family,
            get_in_addr((struct sockaddr *)&cli_addr),
            s, sizeof s);
        unsigned short int p = get_in_port((struct sockaddr *)&cli_addr);
        printf("New connection.\nFrom %s: %u\n\n", s, p);   

        if(fork() == 0) { 
            close(sockfd); // Child don't need listener
            child_process(newsockfd, buffer, 255); 
        }
        close(newsockfd);
    }

    close(sockfd);
    return 0;
}