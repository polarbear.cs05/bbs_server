#ifndef UTIL_H_
#define UTIL_H_

/* This function is called when a system call fails. 
    It displays a message about the error on stderr 
    and then aborts the program. */
void error(char *msg);

/* This function is called when an error occur with MySQL. 
    It displays a message about the error on stderr 
    and then aborts the program. */
void finish_with_error(MYSQL *con);

/* This function is used to clean up zombie processes */
void sigchld_handler(int s);

/* This function get sockaddr, IPv4 or IPv6 */
void* get_in_addr(struct sockaddr *sa);

/* This function get port number, IPv4 or IPv6 */
unsigned short int get_in_port(struct sockaddr *sa);

/* This function loop through linked list of addrinfo structures and bind to the 
    first feasible result */
int loop_and_bind(struct addrinfo *servinfo);

/* This function send message to socket and aborts the program 
    if an error occurs.*/
void send_cnst_msg(int sockfd, char const *msg, size_t len);

/* This function set up server address and bind it to a socket, 
    the file descriptor of the socket is then be returned. 
    This function also set up the SIGCHLD signal handler. */
int initialize(char *portno);

/* This function makes a connection to local database */
void connect_to_database(MYSQL **con);

#endif // UTIL_H_