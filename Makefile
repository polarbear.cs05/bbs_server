CC = gcc
CFLAGS = -pedantic -std=c99 `mysql_config --cflags --libs`
OBJECTS = main.o util.o child.o

all: server

server: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(CFLAGS)

%.o: %.c
	$(CC) -c -o $@ $<

clean:
	-rm $(OBJECTS) server