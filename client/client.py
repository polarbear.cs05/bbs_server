from socket import *
import os
import sys
import threading
    
def handle_receive():
    while True:
        response = clientSocket.recv(4096)
        if response:
            print(response.decode(), end = '')
        else: 
            print("Lost connection")
            clientSocket.close()
            os._exit(1)

if __name__ == '__main__':
    serverName = sys.argv[1]
    serverPort = int(sys.argv[2])
    clientSocket = socket(AF_INET,SOCK_STREAM)
    clientSocket.connect((serverName,serverPort))

    receive_handler = threading.Thread(target=handle_receive,args=())
    receive_handler.start()

    while True:
        content = input()
        clientSocket.send(content.encode())
    
    clientSocket.close()
