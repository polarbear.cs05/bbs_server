# Intro2NP BBS server

A simple BBS server on Linux.

## Commands

***Command Line Prompt: "%"***

### The server accecpts following commands:
When client enter command incompletely E.g., missing parameters, the server will show command format for client.

*  **resgister \<username\> \<email\> \<password\>**

Register with username, email and password. 
\<username\> must be unique. 
\<email\> and \<password\> have no limitation.

*  **login \<username\> \<password\>**

Login with username and password.

*  **logout**

Logout account.

*  **whoami**

Show username if login.

* **create-board \<name\>**

Create a board which named \<name\>.
<name> must be unique.
Must be logged in when creating board’s name.

* **create-post \<board-name\> --title \<title\> --content \<content\>**

(command is in the same line )\
Create a post which title is \<title\> and content is \<content\>.
Use --title and --content to separate titles and content.
\<tilte\> can have space but only in one line.
\<content\> can have space, and key in \<br\> to indicate a new line.
Must be logged in.

* **list-board ##\<key\>**

List all boards in BBS.
\<key\> is a keyword, use ##\<key\> to do advanced query.

* **list-post \<board-name\> ##\<key\>**

List all posts in a board named \<board-name\>
\<key\> is a keyword, use ##\<key\> to do advanced query.

* **read \<post-id\>**

Show the post which ID is \<post-id\>.

* **delete-post \<post-id\>**

Delete the post which ID is \<post-id\>.
Only the post owner can delete the post.

* **update-post \<post-id\> --title/content \<new\>**

Update the post which ID is \<post-id\>.
Use -- to decide which to modify, title or content, and replaced by \<new\>.
Only the post owner can update the post.

* **comment \<post-id\> \<comment\>**

Leave a comment \<comment\> at the post which ID is \<post-id\>.
Must be logged in.

*  **exit**

Close connection.